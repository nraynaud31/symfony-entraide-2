<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Monolog\DateTimeImmutable;


class HomeController extends AbstractController
{

    /**
     * On récupère toutes les infos pour ajouter le post (formulaire),
     * et afficher tous les posts dans la DB
     */
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $posts = $doctrine->getRepository(Post::class)->findAll();

        $reversed_questions = [];

        foreach ($posts as $post) {
            array_push($reversed_questions, $post);
        }

        $revertpost = array_reverse($reversed_questions);

        $user = $this->getUser();
        
        $request = Request::createFromGlobals();
        if($request->request->get('title')){
            $entityManager = $doctrine->getManager();
            $newPost = new Post();
            $newPost->setTitle($request->request->get('title'));
            $newPost->setContent($request->request->get('content'));
            $newPost->setUserId($user);
            $newPost->setCreatedAt(new DateTimeImmutable("Y-m-d"));
            $entityManager->persist($newPost);
            $entityManager->flush();
        }

        return $this->render('home/index.html.twig', [
            'post' => $revertpost,
            'user' => $user,
            'controller_name' => 'HomeController',
        ]);
    }
}
