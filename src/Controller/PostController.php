<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Comment;
use Monolog\DateTimeImmutable;
use App\Entity\Vote;


class PostController extends AbstractController
{

    /**
     * Récup des infos pour ajouter un nouveau commentaire en DB, et le détail du post
     */
    #[Route('/post/{id}', name: 'app_post')]
    public function index(int $id, ManagerRegistry $doctrine): Response
    {
        $post = $doctrine->getRepository(Post::class)->find($id);
        $comment = $post->getComments();
        $user = $this->getUser();

        $request = Request::createFromGlobals();
        if($request->request->get('comment')){
            $entityManager = $doctrine->getManager();
            $newComment = new Comment();
            $newComment->setContent($request->request->get('comment'));
            $newComment->setUserId($user);
            $newComment->setPostId($post);
            $newComment->setCreatedAt(new DateTimeImmutable("Y-m-d"));
            $newComment->setReputation(0);
            $entityManager->persist($newComment);
            $entityManager->flush();
        }
        return $this->render('post/index.html.twig', [
            'post' => $post,
            'comments' => $comment,
            'controller_name' => 'PostController',
        ]);
    }

    #[Route("/delete/{id}" , name:'app_delete')]
    public function delete(int $id, ManagerRegistry $doctrine) {
        $post = $doctrine->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        $entityManager = $doctrine->getManager();

        if($post->getComments() != null){
            $comments = $doctrine->getRepository(Comment::class)->findBy(['post_id' => $post]);

            foreach ($comments as $comment) {
                $entityManager->remove($comment);
                if($comment->getReputation() != null){
                    $votes = $doctrine->getRepository(Vote::class)->findBy(['comment_id' => $comment->getId()]);
                    foreach ($votes as $vote) {
                        $entityManager->remove($vote);
                    }
                }
            }
        }
        
        $entityManager->remove($post);
        $entityManager->flush();

        return $this->redirectToRoute('app_home');
    }

}
