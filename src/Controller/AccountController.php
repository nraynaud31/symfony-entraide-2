<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;



class AccountController extends AbstractController
{
    #[Route('/account', name: 'app_account')]
    public function index(ManagerRegistry $doctrine, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $user = $this->getUser();
        $request = Request::createFromGlobals();
        if($request->request->get('avatar')){
            $entityManager = $doctrine->getManager();
            $newUser = $this->getUser();
            $newUser->setAvatar($request->request->get('avatar'));
            $newUser->setFirstname($request->request->get('firstname'));
            $newUser->setLastname($request->request->get('lastname'));
            $newUser->setPassword($userPasswordHasher->hashPassword($user,$request->request->get('password')));
            $newUser->setJob($request->request->get('job'));
            $entityManager->persist($newUser);
            $entityManager->flush();
        }
        return $this->render('account/index.html.twig', [
            'user' => $user,
            'controller_name' => 'AccountController',
        ]);
    }
}
