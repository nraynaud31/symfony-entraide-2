<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use App\Entity\Comment;
use App\Entity\Vote;
use Doctrine\ORM\EntityManagerInterface;

class VoteController extends AbstractController
{
    #[Route('/post/{id}/vote/{Id}', name: 'app_vote')]
    public function index(int $id, int $Id, ManagerRegistry $doctrine, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        
        $userFound = 0;
        if($user != null){
            $post = $doctrine->getRepository(Post::class)->find($id);
            $votes = $post->getVotes();
            $comments = $doctrine->getRepository(Comment::class)->find($Id);
            foreach ($votes as $vote) {
                if($vote->getUserId() == $user){
                    $userFound = $user->getId();
                }
            }
            
            if($userFound == 0){ 
                $newVote = new Vote(); 
                $newVote->setUserId($user);
                $newVote->setPost($post);
                $newVote->setCommentId($comments);
                $comments->setReputation($comments->getReputation()+1);
                $entityManager->persist($newVote);
                $entityManager->flush();
            }
            return $this->redirectToRoute('app_post', ['id' => $id]);
        }
        
    }
}
